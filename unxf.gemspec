ENV["VERSION"] or abort "VERSION= must be specified"
manifest = File.readlines('.manifest').map! { |x| x.chomp! }
test_files = manifest.grep(%r{\Atest/test_.*\.rb\z})
require 'olddoc'
extend Olddoc::Gemspec
name, summary, title = readme_metadata

Gem::Specification.new do |s|
  s.name = %q{unxf}
  s.version = ENV["VERSION"].dup
  s.homepage = Olddoc.config['rdoc_url']
  s.authors = ["#{name} hackers"]
  s.description = readme_description
  s.email = %q{bofh@bogomips.org}
  s.extra_rdoc_files = extra_rdoc_files(manifest)
  s.files = manifest
  s.rdoc_options = rdoc_options
  s.summary = summary
  s.test_files = test_files
  s.add_dependency('rack', '~> 1.1')
  s.add_development_dependency('olddoc', '~> 1.0')
  s.add_dependency('rpatricia', '~> 1.0')

  s.licenses = %w(GPL-3.0+)
end
